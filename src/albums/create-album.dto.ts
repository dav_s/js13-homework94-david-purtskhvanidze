export class CreateAlbumDto {
  title: string;
  artist: string;
  release: string;
  image: string;
  is_published: boolean;
}
