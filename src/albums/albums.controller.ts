import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Album, AlbumDocument } from '../schemas/album.schema';
import { Model } from "mongoose";
import { CreateAlbumDto } from './create-album.dto';

@Controller('albums')
export class AlbumsController {
    constructor(
        @InjectModel(Album.name) private albumModel: Model<AlbumDocument>,
    ) {}

    @Get()
    getAll() {
        return this.albumModel.find();
    }

    @Get(':id')
    getOne(@Param('id') id: string) {
        return this.albumModel.findById(id);
    }

    @Post()
    create(@Body() albumDto: CreateAlbumDto) {
        const album = new this.albumModel({
            title: albumDto.title,
            artist: albumDto.artist,
            release: albumDto.release,
            image: albumDto.image,
            is_published: albumDto.is_published,
        });

        return album.save();
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.albumModel.findByIdAndDelete(id);
    }


}
