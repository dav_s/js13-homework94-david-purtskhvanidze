import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Track, TrackDocument } from '../schemas/track.schema';
import { Model } from "mongoose";
import { CreateTrackDto } from './create-track.dto';

@Controller('tracks')
export class TracksController {
    constructor(
        @InjectModel(Track.name) private trackModel: Model<TrackDocument>,
    ) {}

    @Get()
    getAll() {
        return this.trackModel.find();
    }

    @Post()
    create(@Body() trackDto: CreateTrackDto) {
        const track = new this.trackModel({
            title: trackDto.title,
            album: trackDto.album,
            duration: trackDto.duration,
            is_published: trackDto.is_published,
        });

        return track.save();
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.trackModel.findByIdAndDelete(id);
    }
}
