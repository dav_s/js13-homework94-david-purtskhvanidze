import mongoose, { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type TrackDocument = Track & Document;

@Schema()
export class Track {
  @Prop({ required: true })
  title: string;

  @Prop({ ref: 'Album', required: true })
  album: mongoose.Schema.Types.ObjectId;

  @Prop()
  duration: string;

  @Prop({ required: true, default: false })
  is_published: boolean;
}

export const TrackSchema = SchemaFactory.createForClass(Track);
