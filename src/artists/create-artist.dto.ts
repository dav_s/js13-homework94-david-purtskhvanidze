export class CreateArtistDto {
  title: string;
  description: string;
  image: string;
  is_published: boolean;
}
