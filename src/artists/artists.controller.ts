import { Body, Controller, Delete, Get, Param, Post, Query } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Artist, ArtistDocument } from '../schemas/artist.schema';
import { Model } from 'mongoose';
import { CreateArtistDto } from './create-artist.dto';

@Controller('artists')
export class ArtistsController {
    constructor(
        @InjectModel(Artist.name) private artistModel: Model<ArtistDocument>,
    ) {}

    @Get()
    getAll() {
        return this.artistModel.find();
    }

    @Get(':id')
    getOne(@Param('id') id: string) {
        return this.artistModel.findById(id);
    }

    @Post()
    create(@Body() artistDto: CreateArtistDto) {
        const artist = new this.artistModel({
            title: artistDto.title,
            description: artistDto.description,
            image: artistDto.image,
            is_published: artistDto.is_published,
        });

        return artist.save();
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.artistModel.findByIdAndDelete(id);
    }

}
